# Story GOAL story_Goal_modeling
+ goal 1614413547112
rests on the analysis of an actor goals, conducted from the point of
view of the actor, by using three basic reasoning techniques: means-end analysis,
contribution analysis, and AND/OR decomposition. 
---
In particular, means-end
analysis aims at identifying plans, resources and softgoals that provide means for
achieving a goal. 
---
Contribution analysis identifies goals that can contribute posi-
tively or negatively in the fulfillment of the goal to be analyzed. In a sense, it can
be considered as an extension of means-end analysis, with goals as means. 
---
AND/OR decomposition-combines AND and OR decompositions of a root goal into
sub-goals, modeling a finer goal structure. Goal modeling is applied to early and
late requirement models in order to refine them and to elicit new dependencies.
---
During architectural design, it contributes to motivate the first decomposition of
the system-to-be actors into a set of sub-actors.