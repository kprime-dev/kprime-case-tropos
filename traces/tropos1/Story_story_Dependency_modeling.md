# Story GOAL story_Dependency_modeling
+ goal 1614413538143
which consists of identifying actors which depend on one
another for goals to be achieved, plans to be performed, and resources to be
furnished. 
---
In particular, in the early requirement phase, it focuses on modeling
goal dependencies between social actors of the organizational setting. New
dependencies are elicited and added to the model upon goal analysis performed
during the goal modeling activity discussed below. 
---
During late requirements
analysis, dependency modeling focuses on analyzing the dependencies of the
system-to-be actor. In the architectural design phase, data and control flows be-
tween sub-actors of the system-to-be actors are modeled in terms of dependencies,
providing the basis for the capability modeling that will start later in architectural
design together with the mapping of system actors to agents.