# Story GOAL story_Actor_modeling
+ goal 1614413527639
+ termbase  tropos_db_db.xml

consists of identifying and analyzing both the actors of the
environment and the system’s Actors and Agents. In particular, in the early
requirement phase actor modeling focuses on modeling the application domain
stakeholders and their intentions as social actors which want to achieve Goals.
---
During late requirement, actor modeling focuses on the definition of the system-to-
be actor, whereas in architectural design, it focuses on the structure of the system-
to-be actor specifying it in terms of sub-systems (actors), interconnected through
data and control flows. 
---
In detailed design, the system’s agents are defined speci-
fying all the notions required by the target implementation platform, and finally,
during the implementation phase actor modeling corresponds to the agent coding.