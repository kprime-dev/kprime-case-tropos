# Story GOAL story_Plan_modeling
+ goal 1614413557240 
can be considered as an analysis technique complementary to goal
modeling. It rests on reasoning techniques analogous to those used in goal
modeling, namely, means-end, contribution analysis and AND/OR decomposi-
tion. In particular, AND/OR decomposition provides an AND and OR decom-
positions of a root plan into sub-plans.