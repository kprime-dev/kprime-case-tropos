# Story facts_on_tropos

add-fact Actor wants_a Goal

add-fact Dependency is-partitioned Plan Resource Goal
add-fact Dependency depends_on Actor
add-fact Dependency required_by Actor
add-fact Dependency may_have_a Why
add-fact Why is-partitioned Plan Resource Goal
add-fact Dependency use Resource
add-fact Actor has Belief
