# Story GOAL story_catch_Early_Requirements
+ goal 1614413015488
---
<pre>
the requirements engineer identifies 
    the domain stakeholders and models them as social actors, 
    who depend on one another 
    for goals to be achieved, 
        plans to be performed, and 
        resources to be furnished.
</pre>
---
<pre>
By clearly defining these dependencies, it is then possible to
    state the why, 
    beside the what and how, 
    of the system functionalities and, 
    as a last result, 
    to verify how the final implementation matches initial needs.
</pre>