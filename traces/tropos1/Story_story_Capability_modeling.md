# Story GOAL story_Capability_modeling
+ goal 1614413567914
starts at the end of the architectural design when system sub-
actors have been specified in terms of their own goals and the dependencies with
other actors. In order to define, choose and execute a plan for achieving its own
goals, each system’s sub-actor has to be provided with specific ‘‘individual’’
capabilities. 
---
Additional ‘‘social’’ capabilities should be also provided for man-
aging dependencies with other actors. Goals and plans previously modeled be-
come integral part of the capabilities. In detailed design, each agent’s capability is
further specified and then coded during the implementation phase.