# Story GOAL story_catch_Architectural_Design
+ goal 1614413045307

    the system’s global architecture 

    in terms of sub-systems,

    interconnected through data and control flows. 
---
Sub-systems are represented, in the model, as actors and 

data/control interconnections are represented as dependencies.
---
The architectural design provides also 

    a mapping of 

    the system actors 
    to a set of software agents, each characterized by specific capabilities.
